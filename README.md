# ShinobiPortable

Portable version of Shinobi built from the Pro repository source code.

## How to use

1. Download Repository. Git clone recommended.

2. copy and rename the following files to enable the executable.
    ```
    conf.sample.json --> conf.json
    super.sample.json --> super.json
    shinobi.sample.sqlite --> shinobi.sqlite
    ```

    Linux and Mac users can quick copy it like this from terminal.
    ```
    cp conf.sample.json conf.json
    cp super.sample.json super.json
    cp shinobi.sample.sqlite shinobi.sqlite
    ```

3. Run the executable for your operating system.
    - **Windows** : Double Click `shinobi-win.exe`
    - **Linux** : Run `chmod +x shinobi-linux && ./shinobi-linux`
    - **MacOS** : Run `chmod +x shinobi-linux && ./shinobi-macos`


## Beyond Testing and moving to Production use

> Shinobi does a lot of stuff in RAM to speed things up and maintain performance on your machine. By default this folder is directed to the root directory of your downloaded repository because we are not running from source.

> To point your temporary directory (`streams`) folder at a different directory you can modify your `"streamDir":""` parameter with an absolute path.

- **Windows** : It is recommended to set this location to an SD card, like `D:/`, to avoid writing temporary data to your main disk drive. You may also use third-party tools to create a RAM drive on windows for the best performance.
- **Linux and MacOS** : set this directory to `/dev/shm`.

### How to use MariaDB or MySQL instead of SQLite3

1. Open the `conf.json` file.

2. Remove the parameter `"databaseType":"sqlite3"`. This will default it to using MariaDB/MySQL.

3. Modify the `"db"` object to point at your database.

4. Insert the provided SQL files in the `sql` folder into your database.
    - framework.sql : Table structure for Shinobi
    - user.sql : Optional, used for connecting to the database with the credentials shown in the `conf.sample.json` file.

5. Restart the Executable.
